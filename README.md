# My Raspberry Pi Config

This configuration allows typical Internet Sharing situation where a connection to the Internet over Wi-Fi is shared to wired-only computers using NAT on a Raspberry Pi.

I'm actively using this configuration on my Raspberry Pi 3.

```
Wi-Fi Router ------- Raspberry Pi ----------+ Wired computers
              Wi-Fi     NAT        Ethernet |
                                            + Wired computers
                                            |
```

## License

Public domain.

